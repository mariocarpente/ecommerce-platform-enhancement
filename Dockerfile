FROM alpine:3.16.2

# Install dependencies
RUN apk add --no-cache \
    git bash curl npm jq \
    build-base libffi-dev \
    openssl-dev bzip2-dev \
    zlib-dev xz-dev readline-dev\
    sqlite-dev tk-dev
    
# Install Pyenv
RUN curl https://pyenv.run | bash
# Export variables to bash profile
RUN echo "export PYENV_ROOT=\"\$HOME/.pyenv\"" >> ~/.bashrc && \
    echo "command -v pyenv >/dev/null || export PATH=\"\$PYENV_ROOT/bin:\$PATH\"" >> ~/.bashrc && \
    echo "eval \"\$(pyenv init -)\"" >> ~/.bashrc && \
    echo "eval \"\$(pyenv virtualenv-init -)\"" >> ~/.bashrc

# Copy Makefile
COPY Makefile .

# Copy requirements
COPY requirements.txt .

RUN /bin/bash -c 'source $HOME/.bashrc; make setup'

ENTRYPOINT ["/bin/bash" ]

