
import requests
from requests_aws4auth import AWS4Auth
import boto3
import time

APPSYNC_API_ENDPOINT_URL = "https://7ojhykxjifc35fy5r5ormcm73e.appsync-api.eu-west-1.amazonaws.com/graphql"
API_KEY = "da2-jhkyq4sbeje6rbqcxj7dueo4ry"

username = "test@test.com"
password = "Prueba123."

client = boto3.client("cognito-idp", region_name="eu-west-1")

# The below code, will do the login
response = client.initiate_auth(
    ClientId="3mbindueqacm7631aa4dbahip5",
    AuthFlow="USER_PASSWORD_AUTH",
    AuthParameters={"USERNAME": username, "PASSWORD": password},
)

token = response["AuthenticationResult"]["AccessToken"]

session = requests.Session()

# Get Product
query = "query MyQuery {getProducts { products { name price productId package { height length weight width }} }}"
response = session.request( url=APPSYNC_API_ENDPOINT_URL, method='POST', headers={'x-api-key': API_KEY}  ,json={'query': query})
product = response.json()['data']['getProducts']['products'][0]
product_name = product['name']
product_id = product['productId']
product_price = product['price']
product_package = product['package']#{height: 50, length: 100, weight: 1000, width: 200}

# Get payment token
url = "https://23gmpfner2.execute-api.eu-west-1.amazonaws.com/prod/preauth"
response = session.request( url=url, method='POST', headers={'x-api-key': "FZJE2lr1Wd3IoSv8AFXev4Y2xXglK72a97G397M2"}, json={'cardNumber': '1234567890123456', 'amount': 3000})
paymentToken = response.json()['paymentToken']

while True:
    # Create order
    mutation = "mutation MyMutation { createOrder(order: {address: {city: \"A Coruña\", country: \"Spain\", name: \"Mario\", phoneNumber: \"6514478\", streetAddress: \"Rua Real\"}, products: {name: \""+product_name+"\", price: "+str(product_price)+", productId: \""+product_id+"\", package: {height: "+str(product_package['height'])+", length: "+str(product_package['length'])+", weight: "+str(product_package['weight'])+", width: "+str(product_package['width'])+"}}, deliveryPrice: 2500, paymentToken: \""+paymentToken+"\"}){ errors message success}}"
    response = session.request( url=APPSYNC_API_ENDPOINT_URL, method='POST', headers={'Authorization': token},json={'query': mutation})
    print(response.json()['data'])
    time.sleep(10)
